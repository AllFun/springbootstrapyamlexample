This test project based on Spring Boot changes:
https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.4-Release-Notes#config-file-processing-application-properties-and-yaml-files

Additional info:
https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-Config-Data-Migration-Guide

Attention to:
https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-Config-Data-Migration-Guide#multi-document-yaml-ordering

If you use multi-document YAML files (files with --- separators) then you need to be aware that property sources are now added in the order that documents are declared. With Spring Boot 2.3 and earlier, the order that the individual documents were added was based on profile activation order.

If you have properties that override each other, you need to make sure that the property you want to "win" is lower in the file. This means that you may need to reorder the documents inside your YAML.