package com.edu;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public abstract class BaseAbstractComponentTest {
    @Value("${test.property}")
    private String property;

    @Test
    void test() {
        assertThat(property).isEqualTo(getExpectedValue());
    }

    abstract String getExpectedValue();
}
