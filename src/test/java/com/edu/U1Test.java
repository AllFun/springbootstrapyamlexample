package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("u1")
public class U1Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u1";
    }
}
