package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"u1", "u2"})
public class U1U2Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u1,u2";
    }
}
