package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("u2")
public class U2Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u2";
    }
}
