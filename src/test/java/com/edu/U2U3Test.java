package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"u2", "u3"})
public class U2U3Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        // here is an issue in config
        // u2 & u3 is overriden by u3
        return "u3";
    }
}
