package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("u3")
public class U3Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u3";
    }
}
