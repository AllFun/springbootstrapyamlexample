package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("u4")
public class U4Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u4";
    }
}
