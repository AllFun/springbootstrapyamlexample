package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"u4", "u5"})
public class U4U5Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        // here is an issue in config
        // u2 & u3 is overriden by u3
        return "u5";
    }
}
