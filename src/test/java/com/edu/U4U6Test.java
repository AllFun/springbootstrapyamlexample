package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"u4", "u6"})
public class U4U6Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u4,u6";
    }
}
