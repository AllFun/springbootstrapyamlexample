package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("u5")
public class U5Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u5";
    }
}
