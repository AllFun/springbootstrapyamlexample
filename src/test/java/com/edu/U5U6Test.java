package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"u5", "u6"})
public class U5U6Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        // here is an issue in config
        // u5 & u6 is overridden by u5 (u6 is skipped because of negation for u5)
        return "u5";
    }
}
