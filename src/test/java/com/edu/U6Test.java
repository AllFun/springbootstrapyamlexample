package com.edu;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("u6")
public class U6Test extends BaseAbstractComponentTest {

    @Override
    String getExpectedValue() {
        return "u6";
    }
}
